﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class NoteHandler : MonoBehaviour
{
    public GameObject noteHolder;
    public Text noteText;
    public bool noteOpened = false;
    public float TheDistance;
    public GameObject ExtraCross;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public CryingSound cryingSound;
    public AudioSource scream;
    public bool cryNote = false;
    public bool spazNote = false;
    public bool darknessNote = false;
    public GameObject spazZom;

    //"Unfair" Note sequence objects
    public Light torchLight;
    public GameObject torchFlame;
    public GameObject hiddenWall;
    public GameObject missingDoor;

   //[UnityEngine.Rendering.PostProcessing.Min(0.1F)]
    //public FloatParameter focusDistance;

    private void Start()
    {
        PostProcessVolume volume = gameObject.GetComponent<PostProcessVolume>();
    }
    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;
        

        if (Input.GetKeyDown(KeyCode.X) && noteOpened)
        {
            Debug.Log("X pressed and noteOpened");
            CloseNotePanel();
        }
    }

    void OnMouseOver()
    {
        if (TheDistance <= 3)
        {
            ExtraCross.SetActive(true);
            ActionDisplay.GetComponent<Text>().text = "Read the note";
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }

        if (Input.GetButtonDown("Action"))
        {
            if (TheDistance <= 3)
            {
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
                Notes nScript = GetComponent<Notes>();
                OpenNotePanel(nScript.noteContents);

                // Changing to blurry background
                //DepthOfField.Settings
                //volume

            }
        }
    }

    void OnMouseExit()
    {
        ExtraCross.SetActive(false);
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }


    void OpenNotePanel(string text)
    {
        
        noteOpened = true;

        Time.timeScale = 0f;

        noteText.text = text;

        noteHolder.SetActive(true);

        if (cryNote)
        {
            cryingSound.Cry();
        }

    }

    IEnumerator Spaz()
    {
        spazZom.SetActive(true);
        yield return new WaitForSeconds(4);
        spazZom.SetActive(false);
    }

    void CloseNotePanel()
    {

        if (spazNote)
        {
            StartCoroutine(Spaz());
            scream.Play();
        }

        if (darknessNote)
        {
            torchLight.enabled = false;
            torchFlame.SetActive(false);
            hiddenWall.SetActive(true);
            missingDoor.SetActive(false);
        }

        noteOpened = false;

        Time.timeScale = 1f;

        noteHolder.SetActive(false);

    }
}
