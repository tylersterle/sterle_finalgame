﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameWin : MonoBehaviour
{
    public float fadeDuration = 5f;
    public GameObject thePlayer;
    public CanvasGroup WinScreen;
    public GameObject ammoScreen;
    public AudioSource jumpMusic;
    public GameObject bunnyHorde;
    public GameObject crosshair;
    float m_Timer;
    bool m_PlayerAtExit = false;



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == thePlayer)
        {
            bunnyHorde.SetActive(false);
            crosshair.SetActive(false);
            jumpMusic.Stop();
            ammoScreen.SetActive(false);
            m_PlayerAtExit = true;
        }
    }

    private void Update()
    {
        if (m_PlayerAtExit)
        {
            WinGame();
        }
    }

    void WinGame()
    {
        m_Timer += Time.deltaTime;
        WinScreen.alpha = m_Timer / fadeDuration;

        if (WinScreen.alpha == 255 && Input.GetKeyDown(KeyCode.Space)) {
            Application.Quit();
        }
    }
}
