﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EZombunnyHorde : MonoBehaviour
{
    public GameObject thePlayer;
    public GameObject BunnyHorde;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == thePlayer)
        {
            BunnyHorde.SetActive(true);
        }
    }
}
