﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class BFirstTrigger : MonoBehaviour
{

    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject TheMarker;
    public GameObject Trigger;
    public GameObject controlPanel;
    public GameObject controlPrompt;

    private void OnTriggerEnter(Collider other)
    {
        this.GetComponent<BoxCollider>().enabled = false;
        StartCoroutine(ScenePlayer());
    }
    
    IEnumerator ScenePlayer()
    {
        controlPrompt.SetActive(false);
        controlPanel.SetActive(false);
        TextBox.GetComponent<Text>().text = "Looks like a gun on that table.";
        TheMarker.SetActive(true);
        yield return new WaitForSeconds(3);
        TextBox.GetComponent<Text>().text = "";
        Trigger.SetActive(false);
    }
}
