﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BZJumpTrigger : MonoBehaviour
{

    public AudioSource DoorBang;
    public AudioSource DoorJumpMusic;
    public AudioSource AmbMusic;
    public GameObject TheZombie;
    public GameObject TheDoor;


    void OnTriggerEnter()
    {
        GetComponent<BoxCollider>().enabled = false;
        TheDoor.GetComponent<Animation>().Play("JumpDoorAnim");
        DoorBang.Play();
        TheZombie.SetActive(true);
        AmbMusic.Stop();
        Debug.Log("Ambient music stopped.");
        DoorJumpMusic.Play();
        Debug.Log("Jumpscare music played.");
        //StartCoroutine(PlayJumpMusic());
    }

    //IEnumerator PlayJumpMusic()
    //{
    //    yield return new WaitForSeconds(0.4f);
    //    AmbMusic.Stop();
    //    Debug.Log("Ambient music stopped.");
    //    DoorJumpMusic.Play();
    //    Debug.Log("Jumpscare music played.");
    //}



}