﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CZJumpTrigger2 : MonoBehaviour
{

    public AudioSource JumpMusic;
    public AudioSource AmbMusic;
    public GameObject TheZombie;
    public GameObject TheDoor;
    public GameObject HiddenWall;


    void OnTriggerEnter()
    {
        GetComponent<BoxCollider>().enabled = false;
        HiddenWall.SetActive(true);
        TheDoor.SetActive(false);
        TheZombie.SetActive(true);
        AmbMusic.Stop();
        JumpMusic.Play();
    }



}