﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DChaseHint : MonoBehaviour
{
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject Trigger;
    private void OnTriggerEnter(Collider other)
    {
        this.GetComponent<BoxCollider>().enabled = false;
        StartCoroutine(Hint());
    }

    IEnumerator Hint()
    {
        TextBox.GetComponent<Text>().text = "I can only slow them down. I need to run, now!";
        yield return new WaitForSeconds(5);
        TextBox.GetComponent<Text>().text = "";
        Trigger.SetActive(false);

    }
}
