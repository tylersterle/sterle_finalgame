﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DZombunnyChase : MonoBehaviour
{
    public GameObject thePlayer;
    public AudioSource jumpMusic;
    public AudioSource ambMusic;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == thePlayer)
        {
            ambMusic.Stop();
            jumpMusic.Play();
        }
    }
}
