﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject controlsPanel;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (controlsPanel.activeSelf)
            {
                controlsPanel.SetActive(false);
            }

            else if (controlsPanel.activeSelf == false)
            {
                controlsPanel.SetActive(true);
            }
            

        }
    }
}
