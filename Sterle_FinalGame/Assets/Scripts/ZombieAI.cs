﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieAI : MonoBehaviour
{
    public GameObject thePlayer;
    public GameObject theEnemy;
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    public bool attackTrigger = false;
    public bool isAttacking = false;
    public int StatusCheck;
    Animator Zombie_Anim;
    public AudioSource hurtSound1;
    public AudioSource hurtSound2;
    public int hurtGen;
    public GameObject hurtFlash;
    public float distance_from_player;
    public int enemyType;


    void Start()
    {
        Zombie_Anim = GetComponent<Animator>();
        if (enemyType == 1)
        {
            navMeshAgent.speed = 1f;
        }
        else if (enemyType == 0)
        {
            navMeshAgent.speed = 4f;
        }
    }


    void Update()
    {
        distance_from_player = Vector3.Distance(theEnemy.transform.position, thePlayer.transform.position);
        transform.LookAt(thePlayer.transform);
        if (attackTrigger == false)
        {
            StatusCheck = 0;
            Zombie_Anim.SetInteger("Status", StatusCheck);
            
            if (distance_from_player <= 8)
            {
                navMeshAgent.SetDestination(thePlayer.transform.position);
            }

        }

        else if (attackTrigger == true && isAttacking == false)
        {
            navMeshAgent.speed = 0;
            StartCoroutine(InflictDamage());
        }
    }

 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            attackTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        attackTrigger = false;
    }

    IEnumerator InflictDamage()
    {
        isAttacking = true;
        hurtGen = Random.Range(1, 3);
        if (hurtGen == 1)
        {
            hurtSound1.Play();
        }

        if (hurtGen == 2)
        {
            hurtSound2.Play();
        }
        hurtFlash.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        hurtFlash.SetActive(false);
        StatusCheck = 1;
        Zombie_Anim.SetInteger("Status", StatusCheck);
        yield return new WaitForSeconds(1.1f);
        GlobalHealth.currentHealth -= 5;
        yield return new WaitForSeconds(0.2f);
        isAttacking = false;
    }
}
