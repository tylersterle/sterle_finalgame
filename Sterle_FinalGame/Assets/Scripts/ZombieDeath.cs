﻿//Jimmy Vegas Unity Tutorials
//This script will give your zombie health


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDeath : MonoBehaviour
{

    Animator Zombie_Anim;
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    public int EnemyHealth = 20;
    public GameObject TheEnemy;
    public int enemyType;
    public int StatusCheck;
    public bool slowDown;
    public AudioSource JumpScareMusic;
    public AudioSource AmbMusic;


    void Start()
    {
        Zombie_Anim = GetComponent<Animator>();

    }
    void DamageZombie(int DamageAmount)
    {
        // Checks to see if the enemy is a Zombie or Zombunny. Zombie takes damage and dies while the Zombunny just slows down.
        if (enemyType == 1)
        {
            EnemyHealth -= DamageAmount;
        }

        else if (enemyType == 2)
        {
            StartCoroutine(SlowDown());
        }

    }

    IEnumerator SlowDown()
    {
        //navMeshAgent.speed = 2.8f;
        navMeshAgent.speed = 1.5f;

        //TODO figure out the right timing for slowing these guys down
        //yield return new WaitForSeconds(1);
        yield return new WaitForSeconds(1);
        navMeshAgent.speed = 4f;
    }




    void Update()
    {
        if (EnemyHealth <= 0)
        {
            StatusCheck = 2;
            Zombie_Anim.SetInteger("Status", StatusCheck);
            this.GetComponent<ZombieAI>().enabled = false;
            TheEnemy.GetComponent<BoxCollider>().enabled = false;
            JumpScareMusic.Stop();
            AmbMusic.Play();
            this.enabled = false;
        }
    }
}