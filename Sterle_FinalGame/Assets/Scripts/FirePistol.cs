﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePistol : MonoBehaviour
{

    public GameObject TheGun;
    public GameObject MuzzleFlash;
    public AudioSource GunFire;
    public AudioSource GunReload;
    public AudioSource GunClick;
    public bool IsFiring = false;
    public float TargetDistance;
    public int DamageAmount = 5;
    public int toFill;
    public bool reloading = false;

    void Update()
    {
        // Firing mechanism
        if (Input.GetButtonDown("Fire1"))
        {
            if (IsFiring == false && reloading == false && GlobalAmmo.ammoCount >= 1)
            {
                StartCoroutine(FiringPistol());
            }

            else if (IsFiring == false && GlobalAmmo.ammoCount == 0){
                GunClick.Play();
            }
        }

        // Reloading mechanism
        if (Input.GetKeyDown(KeyCode.R) && GlobalAmmo.ammoClip != 0 && GlobalAmmo.ammoCount != 7 && reloading == false)
        {
            if (IsFiring == false)
            {
                reloading = true;
                StartCoroutine(ReloadingPistol());
            }
        }

    }

    IEnumerator FiringPistol()
    {
        GlobalAmmo.ammoCount -= 1;
        RaycastHit Shot;
        IsFiring = true;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out Shot)) {
            TargetDistance = Shot.distance;
            Shot.collider.transform.SendMessage("DamageZombie", DamageAmount, SendMessageOptions.DontRequireReceiver);
        }
        TheGun.GetComponent<Animation>().Play("PistolShot");
        MuzzleFlash.SetActive(true);
        MuzzleFlash.GetComponent<Animation>().Play("MuzzleAnim");
        GunFire.Play();
        yield return new WaitForSeconds(0.1f);
        MuzzleFlash.SetActive(false);
        yield return new WaitForSeconds(0.4f);
        IsFiring = false;
    }

    IEnumerator ReloadingPistol()
    {

        GunReload.Play();
        yield return new WaitForSeconds(3.5f);        
        toFill = 7 - GlobalAmmo.ammoCount;
        if(toFill > GlobalAmmo.ammoClip)
        {
            toFill = GlobalAmmo.ammoClip;
            GlobalAmmo.ammoClip = 0;
        }
        else
        {
            GlobalAmmo.ammoClip -= toFill;
        }
     
        GlobalAmmo.ammoCount += toFill;
        reloading = false;
    }
}