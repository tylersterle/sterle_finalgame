﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryingSound : MonoBehaviour
{

    public AudioSource cry;

    // Update is called once per frame
  
    public void Cry()
    {
        StartCoroutine(Crying());
    }
    
    IEnumerator Crying()
    {
            cry.Play();
            yield return new WaitForSeconds(20);
            cry.Stop();
    }
}
