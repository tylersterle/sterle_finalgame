﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalAmmo : MonoBehaviour
{
    public static int ammoCount;
    public static int ammoClip;
    public GameObject ammoCountDisplay;
    public GameObject ammoClipDisplay;
    public int internalAmmoCount;
    public int internalAmmoClip;
    void Update()
    {
        internalAmmoCount = ammoCount;
        internalAmmoClip = ammoClip;
        ammoCountDisplay.GetComponent<Text>().text = "" + ammoCount;
        ammoClipDisplay.GetComponent<Text>().text = "" + ammoClip;
    }
}
